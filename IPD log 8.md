**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**SUB SYSTEM:** PROPULSION AND POWER

**DATE:** 10/12/2021

**AGENDA:**

- We waited for the new motors to arrive, propulsion team is working with other sub systems as well

**DECISIONS:**
- To avoid burning the new motores we were told not to perform thrust test.

*Method/Justification:**
- As the new motors are important to be kept safe from any kind of damange so we deicided not to do thrust test to minimal any risk.

**NEXT STEP:**

-We are discussing our next stem between team members and some are assisting other subsystems.
- Waiting for further instruction of from DR.
