**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**SUB SYSTEM:** PROPULSION AND POWER

**DATE:** 2/12/2021

**AGENDA:**
- worked on calculation of thrust and power.
- Had discussion with DR Ezanee regarding landing of the airship.

**DECISIONS::**
- Made graph of our calculated data.
- Console with Dr Ezanee regarding the issues we are facing and how to solve them.

*Method/Justification:**
- we tried to graphn our results to analyse our calculated data more clearly,
results:

[Power vs velocity](https://gitlab.com/191587/log-book/-/blob/main/Log%202/Screenshot_2021-12-03_151033.pngurl)

[Thrust vs velocity](https://gitlab.com/191587/log-book/-/blob/main/Log%202/Screenshot_2021-12-03_150933.png)

- talked with DR Ezanee regarding landing of aircraft.
- Using thrust vectors of the motor.
or By using a rope a rope (cons, non autonomous and needs human intervention. Besides, it contributes to additional drag to the overall airship. Pros, simple)

**IMPACT:**
We understand more about contribution of different aerodynamic peremeter and about ariship landing etc.

**Next STEP:**

We are still planning for our thrust test and waiting for all equipments to arrive.


