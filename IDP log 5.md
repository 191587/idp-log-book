**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**SUB SYSTEM:** PROPULSION AND POWER

**DATE:** 18/11/2021

**AGENDA:**
To perform thrust test of old motors with similar attributes of our motor used for the spaceship.

**DECISIONS:**
Went to the propulsion lab to test out old motors with similar specifications of [Tarot 4114/320KV Brushless Motor](http://www.tarotrc.com/Product/Detail.aspx?Lang=en&Id=7caf622a-6119-4aaf-90e2-ed846acfb13f)


**Method/Justification:**

 
- We tested all 4 old motors

- out of 4 , 2 motors were functioning,

- One motor was in good condition and the max thrust and current were T = 1.00kg, I = 6.13A

   gathered data for the motor:

   [current vs time](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/motor_2_current.png)


   [Thrust vs Time](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/motor_2_thrust.png)




- Another motor was functioning but had vibration, the thrust and current measured were respectively T = 0.49kg, I = 12.70A

[current vs time](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/motor_3_current.png)


[Thrust vs Time](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/motor_3_thrust.png)


- All motors were tested using 17 inch propeller.


**IMPACT:**

- We were able to do the thrust test with minimal issues.

- Dr. Ezanee gave a brief explanantion on how we should be able to find the thrust that is required from the equilibrium of forces of the airship. [Thrust](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/uploads/b97898bac28243ae46bc53ce70aac771/NOTE_1601121.pdf)

**NEXT STEP:**
- Waiting for Aerodynamic data from Simulation team to do Therotical thrust and power calculation.
