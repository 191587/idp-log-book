**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**SUB SYSTEM:** PROPULSION AND POWER

**DATE:** 12/11/2021

**AGENDA:**
- Went to propusion lab to carry out thrust test to get more familiar with thrust testing and planning for our thrust test.

- Got through the fundamental of Airship text book by DR Salah

**DECISIONS:**
- We wanted to go to the lab to do the thtust test again as the previous week the testing wasnt successful and we wanted to get more familiar with the thrust testing procedure and software.
- Team members tried to perform Trolly test as a method of thrust test.

**Method/Justification:**
- tried to test out old motors so that we can perform the test successfully when the new motors arrive.

- Also propulsion team tried to perform out trolly testing but there were a lot of issues regarding the experminent.

**IMPACT:**

- We discussed with DR Ezanee and he suggested us to calulate thrust manually using Thrust formula.

- We need to obtain Data from other sub system to do the manual calculation.

**NEXT STEP:** Need to go through therotical approach for Thrust test and also be more familiar with our motor performance.
