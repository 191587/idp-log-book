**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**SUB SYSTEM:** PROPULSION AND POWER

**DATE:** 05/11/2021


**AGENDA/GOALS:**

- Getting familar with Gitlab, 2 assisngments was given by Mr Azizi for us regarding Gitlab.

- Created Ghantt Chart for planning our work properly and be updated with work of other sub systems.

**DECISIONS:**
- We had meetings among our team memebers and decided how we are going to finish our tasks for our sub system and came up with the time frame.

- all sub systems created ghantt chart and google spreadsheet so that we can keep updated as in the project most of the sub systems are depended on each other.

**Method/Justification:**

- We decided to visit the propulsion Lab to do some testing of the motors.
- We will use the ghantt chart to keep other sub system updated about our progress.

**IMPACT:**

- Because of Ghantt Chart all sub systems now have a time frame of their work it will help us the plan properly.

**NEXT STEP:** 

- Visit the laboratory to do some testing and get familiar with our equipments.

