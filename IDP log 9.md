**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**SUB SYSTEM:** PROPULSION AND POWER

**DATE:** 24/12/2021

**AGENDA:**
- Test the new arrived motor

**DECISIONS:**
- Team decided to undergo the test the same day flight integral team will work in compiling the parts.

**Method/Justification:**
- Test using power supply of 22v
- Members Tried to make the test as safe as possible to avoid burn out

Result:

Thrust= 1KG 
current = 2.3 A

**Impact:** 
By the thrust test now we know the performance of the motor which is important for us for the project.

**NEXT STEP**
Propulstion team has done almost all necessary analysis required for the project, mow we will start writing our report.
