**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**SUB SYSTEM:** PROPULSION AND POWER

**DATE:** 05/11/2021

**AGENDA:**
- Get familair with the thrust testing and perform thrust test of the old motors.

**DECISIONS:**

- We decided to got to the lab and to get familiar with equipment and thrust test ny testing with motor KV340 with propeller of 16 inch and 24 inch.

[Specification of the motor](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/Motor_Test_Report.png)

**Method/Justification:**

- We tested the motor using the thrust test but find our the motors are not in very good conditions, the thrust produced was not sufficient.

- Propusltion team tried testing both 17inch and 23 inch propeller but the 23inch propeller burnt during the testing around 50% of the thruttle.

**IMPACT:**
- We have an Idea regarding the thrust test process which we need to do for our project. Its important for us to know how to do the thrust test so that we can do the testing of our new motors accurately and without any problem.

- Leanred about thrust testing set ups and softwares and equipments of RCBenchmark.

**CURRENT ISSUES:**

- We are still trying to figure out and planning regarding our tasks to make the project successful.Still we are facing some issues, the Thrurst test wasnt successful, we are discussing further with Dr ezane.

**Next step:**

- We are discussing with Dr Ezanee and Amirul Fikri regarding how we can perform the Thurst taste and figure out the Thrust required for our model and the size of the propeller which would be needed.
- Will perform thrust test again next week.
