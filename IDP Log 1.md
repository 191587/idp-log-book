**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**SUB SYSTEM:** PROPULSION AND POWER

**DATE:** 29/10/2021


**AGENDA/GOALS:**
- this week were introduced to our Aerospace Design Project for this semester.
- Our goal is to design and develop an Airship.
- We are divided in to eight sub system groups and each sub system has some specific tasks to perform.
-  A workshop  by Mr. Azizi on how to use GitLab have been conducted to introduce us with gitlab.

**DECISIONS:**
- This week we are being divided in to our Sub systems and smaller groups.

[Subsystems and groups](https://gitlab.com/irfanfairuz/idp-airship-development/-/raw/main/Image/substym_n_team.jpeg)

**Method/Justification:** NA

**IMPACT:**

- with being assigned in smaller groups ans sub system we can easily divide our tasks between each other and do the specific tasks.
- Everyone can be easily updated by the progress of different subsystems.

**NEXT STEP:** 

- we Started discussion and planning among our subsystem group that how we going to complete our task.
