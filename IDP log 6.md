**NAME:** SYED ASIF UZ ZAMAN

**MATRIC:** 191587

**SUB SYSTEM:** PROPULSION AND POWER

**DATE:** 26/11/2021

**AGENDA:**

- To do Theorotical Thrust calculation using data provided by other sub system and also perform some Thrust analysis.

**DECISIONS::**
- We recieved aerodynamic data from other subsystem which we will use to theorotically calculate Thrust.
- Team also permormed Thrust test in the lab to get more data.

**Method/Justification:**

- We got aerodynamic data from Simulation team and also from design team we got the specification of the air craft we used general formula to calculate thrust and power from it so that we can analyse the data.

-Theorotical thrust and power calculation result: [result](https://gitlab.com/191587/log-book/-/blob/main/Screenshot_2021-11-26_150636.png)

-Test 2nd motor with power supply (16V) and also battery (15.8V) with ESC

**IMPACT:**
- From the result from the theorotical calculation we can know the amount of thrust and power required for our airship and compare with the power and thrust of our motor.

**ISSUES:**
- the values of CL and CD given to us by the simulation team seems not accurate as from the thrust calculation the result we got doesnt look correct.

**NEXT STEP:**
- Further inverstigation required to understand the problem regarding theorotical thrust calcultation. 





